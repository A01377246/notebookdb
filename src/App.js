import React, { useState, useEffect } from "react";
import "./App.css";
import Activities from "./components/Activities";
import NewActivity from "./components/NewActivity/NewActivity";
import { db } from "./firebaseConfig";
import { collection, getDocs } from "firebase/firestore";
import "./resources/images/fondoLab.png";

/*<div className="App">
      <header className="App-header">
        <img src={loadingBottle} className="App-logo" alt="logo" />
        <p>IGEM TEC CEM</p>
        <ExpenseItem />
      </header>
    </div>*/

const activityCollectionReference = collection(db, "Activities");

// Your web app's Firebase configuration

function App() {
  const [activities, setActivities] = useState([]);

  useEffect(() => {
    const getActivities = async () => {
      const data = await getDocs(activityCollectionReference);
      setActivities(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
      console.log(data);
    };
    getActivities();
  }, []);

  const addActivityHandler = (activity) => {
    setActivities((prevActivities) => {
      return [activity, ...prevActivities];
    });
  };

  return (
    <div className="labBg">
      <NewActivity onAddActivity={addActivityHandler} />
      <Activities items={activities} />
    </div>
  );
}

export default App;
