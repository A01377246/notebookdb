import "./ExpenseForm.css";
import React, { useState } from "react";

function ExpenseForm(props) {
  //Multiple states can be used or a single used state can receive an object that contains the the different values that will be stored
  const [enteredActivityTitle, setEnteredActivityTitle] = useState("");
  const [enteredAmount, setEnteredAmount] = useState("");
  const [enteredDate, setEnteredDate] = useState("");

  const titleChangeHandler = (event) => {
    setEnteredActivityTitle(event.target.value);
  };

  const amountChangeHandler = (event) => {
    setEnteredAmount(event.target.value);
  };

  const dateChangeHandler = (event) => {
    setEnteredDate(event.target.value);
  };

  const submbitHandler = (event) => {
    event.preventDefault(); //Prevents the oage from reloading when the submit button is clicked

    const expenseData = {
      activityName: enteredActivityTitle,
      amount: enteredAmount,
      date: new Date(enteredDate),
    };

    //Clear input fields by using two way binding (passing a value back to the input)

    props.onSaveExpenseData(expenseData); // call a function defined on the parent component
    setEnteredActivityTitle("");
    setEnteredAmount("");
    setEnteredDate("");
  };

  return (
    <form onSubmit={submbitHandler}>
      <div className="new-expense__controls">
        <div className="new-expense__control">
          <label>Activity Title</label>
          <input
            type="text"
            value={enteredActivityTitle}
            onChange={titleChangeHandler}
          />
        </div>
        <div className="new-expense__control">
          <label>Amount</label>
          <input
            type="number"
            min="0.01"
            step="0.01"
            onChange={amountChangeHandler}
            value={enteredAmount}
          />
        </div>
        <div className="new-expense__control">
          <label>Date</label>
          <input
            type="date"
            min="2022-01-01"
            max="2022-12-31"
            onChange={dateChangeHandler}
            value={enteredDate}
          />
        </div>
      </div>
      <div className="new-expense__actions">
        <button type="submbit">Add Expense</button>
      </div>
    </form>
  );
}

export default ExpenseForm;
