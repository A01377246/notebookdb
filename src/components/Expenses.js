import "./Expenses.css";
import ExpenseItem from "./ExpenseItem";
import Card from "./Card";
import Monthfilter from "./MonthFilter/MonthFilter";
import React, { useState } from "react";

function Expenses(props) {
  const [filteredMonth, setFilteredMonth] = useState("February");

  const MonthSelectionHandler = (month) => {
    setFilteredMonth(month);
  };

  return (
    <div>
      <Card className="expenses">
        <Monthfilter
          selected={filteredMonth}
          onMonthSelection={MonthSelectionHandler}
        />
        {props.items.map((expense) => (
          <ExpenseItem
            key={expense.id}
            activityName={expense.activityName}
            area={expense.area}
            date={new Date(expense.date)}
          ></ExpenseItem>
        ))}
      </Card>
    </div>
  );
}

export default Expenses;
